# StyleGAN
**StyleGAN Implementation**

The purpose of this project is to provide a simplified TensorFlow implementation of Style Generative Adversarial Network, or StyleGAN for short, capable of generating impressively photorealistic high-quality photos of faces. 

It is an extension to the GAN architecture that proposes large changes to the generator model, including the use of a mapping network to map points in latent space to an intermediate latent space, the use of the intermediate latent space to control style at each point in the generator model, and the introduction to noise as a source of variation at each point in the generator model.

Official Article and Repo available at: 

- [https://arxiv.org/abs/1812.04948](url)
- [https://github.com/NVlabs/stylegan](url)
